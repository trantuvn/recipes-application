//
//  IngredientTableViewCell.swift
//  RecipeApp
//
//  Created by Tu (Tony) A. TRAN on 1/8/20.
//  Copyright © 2020 Tu (Tony) A. TRAN. All rights reserved.
//

import UIKit

class IngredientTableViewCell: UITableViewCell {

    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
