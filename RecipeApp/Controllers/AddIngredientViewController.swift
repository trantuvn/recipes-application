//
//  AddIngredientViewController.swift
//  RecipeApp
//
//  Created by Tu (Tony) A. TRAN on 1/8/20.
//  Copyright © 2020 Tu (Tony) A. TRAN. All rights reserved.
//

import UIKit
import PKHUD

protocol AddItemControllerDelegate {
    func didFinishAddItem(_ item: Any)
    func didFinishEditItem(_ item: Any)
    func didCancelEditItem()

}

class AddIngredientViewController: UIViewController, UITextFieldDelegate {
    var delegate:AddItemControllerDelegate?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var typeTextfield: UITextField!
    @IBOutlet weak var quantityTextfield: UITextField!
    
    @IBOutlet weak var doneButton: UIButton!
    
    var editingIngredient:Ingredient?
    @IBOutlet weak var gestureView: UIView!

    // MARK: - Life circle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if editingIngredient != nil {
            nameTextfield.text = editingIngredient?.name
            typeTextfield.text = editingIngredient?.type
            quantityTextfield.text = editingIngredient?.quantity
            
            titleLabel.text = "Edit Ingredient"
        }
        else {
            titleLabel.text = "Add Ingredient"
        }
        
        doneButton.isEnabled = self.validateData()
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(gestureViewDidTap))
        gestureView.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func gestureViewDidTap() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.didCancelEditItem()
      }
      
    
    // MARK: - Methods

    func validateData() -> Bool {
        let validName = !(nameTextfield.text?.isEmpty ?? true)
        let validType = !(typeTextfield.text?.isEmpty ?? true)
        let validQuantity = !(quantityTextfield.text?.isEmpty ?? true)

        return validName && validType && validQuantity
    }
    
    // MARK: - IBAction

    @IBAction func doneButtonTapped(_ sender: Any) {
        let newIngredient = Ingredient()
        newIngredient.name = nameTextfield.text ?? ""
        newIngredient.type = typeTextfield.text ?? ""
        newIngredient.quantity = quantityTextfield.text ?? ""
        
        if editingIngredient != nil {
            self.delegate?.didFinishEditItem(newIngredient)
        }
        else {
            self.delegate?.didFinishAddItem(newIngredient)
        }

        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UITextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        doneButton.isEnabled = self.validateData()
        
        return true
    }
}

