//
//  AddStepViewController.swift
//  RecipeApp
//
//  Created by Tu (Tony) A. TRAN on 1/8/20.
//  Copyright © 2020 Tu (Tony) A. TRAN. All rights reserved.
//

import UIKit
import PKHUD

class AddStepViewController: UIViewController, UITextViewDelegate {
    
    var editingStep:String?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var stepTextView: UITextView!
    @IBOutlet weak var doneButton: UIButton!
    var delegate:AddItemControllerDelegate?
    
    @IBOutlet weak var gestureView: UIView!
    // MARK: - Life Circle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !(editingStep?.isEmpty ?? true) {
            titleLabel.text = "Edit Step"
            stepTextView.text = editingStep
        }
        else {
            titleLabel.text = "Add Step"
        }
        
        doneButton.isEnabled = self.validateData()
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(gestureViewDidTap))
        gestureView.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func gestureViewDidTap() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.didCancelEditItem()
    }
    
    // MARK: - Methods
    
    func validateData() -> Bool {
        let validStep = !(stepTextView.text?.isEmpty ?? true)
        return validStep
    }
    
    // MARK: - IBAction
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        if editingStep != nil {
            self.delegate?.didFinishEditItem(stepTextView.text ?? "")
        }
        else {
            self.delegate?.didFinishAddItem(stepTextView.text ?? "")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UITextViewDelegate
    
    func textViewDidChange(_ textView: UITextView) {
        doneButton.isEnabled = self.validateData()
    }
}
