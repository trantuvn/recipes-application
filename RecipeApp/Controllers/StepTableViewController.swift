//
//  StepTableViewController.swift
//  RecipeApp
//
//  Created by Tu (Tony) A. TRAN on 1/8/20.
//  Copyright © 2020 Tu (Tony) A. TRAN. All rights reserved.
//

import UIKit
import RealmSwift
import PKHUD

class StepTableViewController: UITableViewController {
    var steps = [String]()
    var enableEdit = false
    var editingIndex = 0
    
    // MARK: - Life circle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")
        tableView.estimatedRowHeight = 100
    }
    
    // MARK: - Table view data source & delegate
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if enableEdit {
            return steps.count + 1
        }
        
        return steps.count
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if enableEdit && indexPath.row == steps.count {
            //Add a cell to the end of table to add new Ingredient
            let cell = UITableViewCell()
            cell.textLabel?.text = "Tap to Add New Step"
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 20)
            cell.textLabel?.textAlignment = .center
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
            
            let step = steps[indexPath.row]
            
            cell.textLabel?.numberOfLines = 0
            cell.detailTextLabel?.numberOfLines = 0

            cell.textLabel?.text = "\(indexPath.row + 1). " + step
            cell.detailTextLabel?.text = ""

            return cell
        }
    }
    
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return enableEdit
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
            steps.remove(at: indexPath.row)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if enableEdit {
            let addController = AddStepViewController(nibName: "AddStepViewController", bundle: Bundle.main)
            addController.delegate = self
            
            if indexPath.row != steps.count  {
                let step = steps[indexPath.row]
                addController.editingStep = step
                editingIndex = indexPath.row
            }
            
            self.present(addController, animated: true, completion: nil)
        }
    }
}

extension StepTableViewController: AddItemControllerDelegate {
    func didFinishAddItem(_ item: Any) {
        steps.append(item as! String)
        tableView.reloadData()
        
        self.resignFirstResponder()
        HUD.flash(.success)
    }
    
    func didFinishEditItem(_ item: Any) {
        steps[editingIndex] = item as! String
        tableView.reloadData()
        
        self.resignFirstResponder()
        HUD.flash(.success)
    }
    
    func didCancelEditItem() {
         self.resignFirstResponder()
     }
}
