//
//  ViewController.swift
//  RecipeApp
//
//  Created by Tu (Tony) A. TRAN on 1/8/20.
//  Copyright © 2020 Tu (Tony) A. TRAN. All rights reserved.
//

import UIKit
import RealmSwift
import PKHUD

class RecipesListViewController: UIViewController {
    
    var recipeType: [RecipeCategory]?
    var displayingType: RecipeCategory?
    var displayingRecipes: [Recipe]?
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: "RecipeCellID")
        }
    }
    
    @IBOutlet weak var filterBarButton: UIBarButtonItem!
    @IBOutlet weak var addBarButton: UIBarButtonItem!
    
    // MARK: - Life circle
    override func viewDidLoad() {
        super.viewDidLoad()
   
        self.getDataFromXML()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //To make sure new item appear after add new recipe
        if displayingType != nil {
            self.reloadTableViewData(displayingType)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "enterDetailsView" {
            if let detailController = segue.destination as? RecipeDetailsViewController {
                detailController.recipe = sender as? Recipe
                detailController.displayingType = self.displayingType
            }
        }
    }
    
    // MARK: - Methods
    func getDataFromXML()  {
        let parse = XMLParserHelper()
        parse.delegate = self
        parse.getDataFromFile("recipetypes")
    }
    
    func getDataFromJson()  {
        NetworkServices.getRecipesDataFromJSON {[weak self] (recipes, error) in
            self?.recipeType = recipes
            self?.reloadTableViewData(recipes?.first)
        }
    }
    
    func reloadTableViewData(_ type: RecipeCategory!) {
        HUD.show(.progress)
        
        self.displayingType = type
        displayingRecipes = RealmServices.getRecipeOfCategoryID(type!.id)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
        HUD.flash(.success)
    }
    
    // MARK: - IBAction
    @IBAction func filterButtonDidTap(_ sender: Any) {
        pickerView.isHidden = !pickerView.isHidden
    }
    
    @IBAction func addButtonDidTap(_ sender: Any) {
        performSegue(withIdentifier: "enterDetailsView", sender: nil)
    }
    
}

extension RecipesListViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return recipeType?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let text = recipeType?[row].name ?? ""
        return text
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let newType = recipeType?[row]
        self.reloadTableViewData(newType)
        
        pickerView.isHidden = true
    }
}

extension RecipesListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return displayingType?.name ?? "There are no available items!"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayingRecipes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCellID", for: indexPath)
        if let recipe = displayingRecipes?[indexPath.row] {
            cell.textLabel?.text = recipe.name
            cell.detailTextLabel?.text = recipe.desc
            
            if let image = UIImage(contentsOfFile: recipe.imageURL) {
                cell.imageView?.image = image
            }
            else {
                cell.imageView?.image = UIImage(named: "recipe_holder")
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let recipe = displayingRecipes?[indexPath.row] {
            performSegue(withIdentifier: "enterDetailsView", sender: recipe)
        }
    }
}

extension RecipesListViewController: XMLParserHelperDelegate {
    func parserDidEndDocument(_ result: [RecipeCategory]) {
        self.recipeType = result
        self.reloadTableViewData(result.first)
    }
}
