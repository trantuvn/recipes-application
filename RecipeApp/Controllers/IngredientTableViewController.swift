//
//  IngredientTableViewController.swift
//  RecipeApp
//
//  Created by Tu (Tony) A. TRAN on 1/8/20.
//  Copyright © 2020 Tu (Tony) A. TRAN. All rights reserved.
//

import UIKit
import RealmSwift
import PKHUD

class IngredientTableViewController: UITableViewController {
    var ingredients = [Ingredient]()
    var enableEdit = false
    var editingIndex = 0

    // MARK: - Life circle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.clearsSelectionOnViewWillAppear = false
        tableView.register(UINib(nibName: "IngredientTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "IngredientTableViewCellID")
        tableView.estimatedRowHeight = 100
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if enableEdit {
            return ingredients.count + 1
        }
        
        return ingredients.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if enableEdit && indexPath.row == ingredients.count {
            //Add a cell to the end of table to add new Ingredient
            let cell = UITableViewCell()
            cell.textLabel?.text = "Tap to Add New Ingredient"
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 20)
            cell.textLabel?.textAlignment = .center
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "IngredientTableViewCellID", for: indexPath) as! IngredientTableViewCell
            
            let ingredient = ingredients[indexPath.row]
            cell.nameLabel?.text = ingredient.name
            cell.quantityLabel?.text = ingredient.quantity + "\n" + ingredient.type
            
            return cell
        }
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return enableEdit
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
            ingredients.remove(at: indexPath.row)
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if enableEdit {
            let addController = AddIngredientViewController(nibName: "AddIngredientViewController", bundle: Bundle.main)
                      addController.delegate = self
                      
            if indexPath.row != ingredients.count  {
                let ingredient = ingredients[indexPath.row]
                addController.editingIngredient = ingredient
                editingIndex = indexPath.row
            }
            
           self.present(addController, animated: true, completion: nil)
        }
    }
}

extension IngredientTableViewController: AddItemControllerDelegate {
    func didFinishAddItem(_ item: Any) {
        ingredients.append(item as! Ingredient)
        tableView.reloadData()
        self.resignFirstResponder()
        HUD.flash(.success)
    }
    
    func didFinishEditItem(_ item: Any) {
        ingredients[editingIndex] = item as! Ingredient
        tableView.reloadData()
        self.resignFirstResponder()
        HUD.flash(.success)
    }
    
    func didCancelEditItem() {
        self.resignFirstResponder()
    }
}
