//
//  RecipeDetailsViewController.swift
//  RecipeApp
//
//  Created by Tu (Tony) A. TRAN on 1/8/20.
//  Copyright © 2020 Tu (Tony) A. TRAN. All rights reserved.
//

import UIKit
import RealmSwift
import Photos
import PKHUD

class RecipeDetailsViewController: UIViewController {
    @IBOutlet weak var parentStackView: UIStackView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var editBarButton: UIBarButtonItem!
    
    @IBOutlet weak var editImageButton: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var recipeNameTextField: UITextField!
    
    weak var recipe:Recipe?
    weak var displayingType:RecipeCategory!
    
    var stepController = StepTableViewController()
    var ingredientController = IngredientTableViewController()
    
    var isEditingRecipe:Bool = false {
        didSet{
            if isEditingRecipe {
                self.enableEditMode()
            }
            else {
                self.disableEditMode()
            }
        }
    }
    
    var newImagePath:String?
    
    // MARK: - Life circle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if recipe == nil {
            //Enable adding mode
            isEditingRecipe = true
        }
        else {
            setupDefaultUI()
        }
        
        stepController.steps = recipe?.steps.toArray(type: String.self) ?? [String]()
        ingredientController.ingredients = recipe?.ingredients.toArray(type: Ingredient.self) ?? [Ingredient]()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(deviceOrientationDidChange), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateStackView()
    }
    
    // MARK: - Methods
    @objc func deviceOrientationDidChange(_ notification: Notification) {
        self.updateStackView()
    }
    
    func updateStackView()  {
        let orientation = UIApplication.shared.statusBarOrientation
        if orientation.isPortrait {
            parentStackView.axis = .vertical
            parentStackView.distribution = .fill
        } else {
            parentStackView.axis = .horizontal
            
            let device = UIDevice.current.userInterfaceIdiom
            if device == .pad {
                parentStackView.distribution = .fillEqually
            }
            else{
                parentStackView.distribution = .fill
            }
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func showDescription() {
        stepController.view.isHidden = true
        ingredientController.view.isHidden = true
        
        textView.isHidden = false
    }
    
    func showSteps() {
        stepController.view.translatesAutoresizingMaskIntoConstraints = false
        stepController.view.frame = contentView.bounds
        stepController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        if stepController.view.superview == nil {
            self.addChild(stepController)
            contentView.addSubview(stepController.view)
        }
        
        textView.isHidden = true
        ingredientController.view.isHidden = true

        stepController.view.isHidden = false
    }
    
    func showIngredients() {
        ingredientController.view.translatesAutoresizingMaskIntoConstraints = false
        ingredientController.view.frame = contentView.bounds
        ingredientController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        if ingredientController.view.superview == nil {
            self.addChild(ingredientController)
            contentView.addSubview(ingredientController.view)
        }
        
        textView.isHidden = true
        stepController.view.isHidden = true
        
        ingredientController.view.isHidden = false
    }
    
    func setupDefaultUI()  {
        self.title = "Recipe Detail"
        textView.text = recipe?.desc ?? "Enter description here"
        recipeNameTextField.text = recipe?.name
        
        if let image = UIImage(contentsOfFile: recipe?.imageURL ?? "") {
            imageView.image = image
        }
        else {
            imageView.image = UIImage(named: "recipe_holder")
        }
        
        self.disableEditMode()
    }
    
    func enableEditMode() {
        stepController.enableEdit = isEditingRecipe
        stepController.tableView.reloadData()
        
        ingredientController.enableEdit = isEditingRecipe
        ingredientController.tableView.reloadData()
        
        editBarButton.title = "Save"
        recipeNameTextField.isEnabled = true
        editImageButton.isHidden = false
        textView.isEditable = true
        
        if recipe?.name.isEmpty ?? true {
            recipeNameTextField.placeholder = "Tap To Edit Name"
        }
    }
    
    func disableEditMode() {
        stepController.enableEdit = isEditingRecipe
        stepController.tableView.reloadData()
        
        ingredientController.enableEdit = isEditingRecipe
        ingredientController.tableView.reloadData()
        
        stepController.enableEdit = isEditingRecipe
        ingredientController.enableEdit = isEditingRecipe
        
        editBarButton.title = "Edit"
        recipeNameTextField.isEnabled = false
        editImageButton.isHidden = true
        textView.isEditable = false
        
        recipeNameTextField.text = recipe?.name
        
        self.resignFirstResponder()
    }
    
    // MARK: - IBAction

    @IBAction func editImageButtonTaped(_ sender: Any) {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.allowsEditing = false
        pickerController.sourceType = .photoLibrary
        
        present(pickerController, animated: true, completion: nil)
    }
    
    @IBAction func segmentControlValueChanged(_ sender: Any) {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            showDescription()
        case 1:
            showIngredients()
        case 2:
            showSteps()
        default:
            showDescription()
        }
    }
    
    @IBAction func barButtonItemTaped(_ sender: Any) {
        if isEditingRecipe {
            let newRecipe = Recipe()
            newRecipe.name = recipeNameTextField.text ?? "New recipe"
            newRecipe.desc = textView.text ?? "New recipe descprtion"
            newRecipe.ingredients.append(objectsIn: ingredientController.ingredients)
            newRecipe.steps.append(objectsIn: stepController.steps)
            newRecipe.imageURL = newImagePath ?? ""
            newRecipe.cateforyID = displayingType.id
            
            if recipe == nil {
                newRecipe.create()
                recipe = newRecipe
            }
            else {
                recipe?.save(newRecipe)
            }
            
            HUD.flash(.success)
        }
   
        isEditingRecipe = !isEditingRecipe
    }
}

extension RecipeDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.contentMode = .scaleAspectFit
            imageView.image = pickedImage
            
           
            if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL{
                let imgName = imgUrl.lastPathComponent
                let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
                let localPath = documentDirectory?.appending(imgName)
                
                let data = pickedImage.pngData()! as NSData
                data.write(toFile: localPath!, atomically: true)
                newImagePath = localPath
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
}

