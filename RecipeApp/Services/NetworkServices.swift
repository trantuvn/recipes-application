//
//  NetworkServices.swift
//  RecipeApp
//
//  Created by Tu (Tony) A. TRAN on 1/8/20.
//  Copyright © 2020 Tu (Tony) A. TRAN. All rights reserved.
//

import UIKit

class NetworkServices: NSObject {
    
    static func getRecipesDataFromJSON(onComplete: @escaping ([RecipeCategory]?, Error?) -> Void) {
        if let path = Bundle.main.path(forResource: "recipetypes", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                
                let decoder = JSONDecoder()
                let result = try decoder.decode([RecipeCategory].self, from: data)
                onComplete(result, nil)
            } catch let error {
                onComplete (nil, error)
            }
        }
    }
}
