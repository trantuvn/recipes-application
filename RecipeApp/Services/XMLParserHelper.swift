//
//  XMLParser.swift
//  RecipeApp
//
//  Created by Tu (Tony) A. TRAN on 1/8/20.
//  Copyright © 2020 Tu (Tony) A. TRAN. All rights reserved.
//

import UIKit

protocol XMLParserHelperDelegate {
    func parserDidEndDocument(_ result:[RecipeCategory])
}


class XMLParserHelper: NSObject {
    var delegate:XMLParserHelperDelegate?

    var recipeType: [RecipeCategory] = []
    var elementName: String = String()
    var typeID = String()
    var typeName = String()
    
    func getDataFromFile(_ fileName:String) {
        if let path = Bundle.main.url(forResource: fileName, withExtension: "xml") {
            if let parser = XMLParser(contentsOf: path) {
                parser.delegate = self
                parser.parse()
            }
        }
    }
}

//Category
extension XMLParserHelper: XMLParserDelegate{
    // 1
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {

        if elementName == "type" {
            typeID = String()
            typeName = String()
        }

        self.elementName = elementName
    }

    // 2
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "type" {
            let type = RecipeCategory()
            type.id = typeID
            type.name = typeName
            recipeType.append(type)
        }
    }

    // 3
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        if (!data.isEmpty) {
            if self.elementName == "name" {
                typeName += data
            }
            else if self.elementName == "id" {
                typeID += data
            }
        }
    }
    
    
    func parserDidEndDocument(_ parser: XMLParser) {
        self.delegate?.parserDidEndDocument(recipeType)
    }
}
