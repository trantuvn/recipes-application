//
//  RealmServices.swift
//  RecipeApp
//
//  Created by Tu (Tony) A. TRAN on 1/8/20.
//  Copyright © 2020 Tu (Tony) A. TRAN. All rights reserved.
//

import UIKit
import RealmSwift

extension Realm {
    public func safeWrite(_ block: (() throws -> Void)) throws {
        if isInWriteTransaction {
            try block()
        } else {
            try write(block)
        }
    }
}

extension Results {
    func toArray<T>(type: T.Type) -> [T] {
        return compactMap { $0 as? T }
    }
}


extension List {
    func toArray<T>(type: T.Type) -> [T] {
        return compactMap { $0 as? T }
    }
}

class RealmServices {
    
    static func createRecipe (_ recipe:Recipe){
        let realm = try! Realm()
        let lastID = realm.objects(Recipe.self).count + 1
        recipe.id = "\(lastID)"
        
        try! realm.safeWrite{
            realm.add(recipe, update: .all)
        }
    }
    
    static func saveRecipe(_ recipe:Recipe, newData:Recipe)  {
        let realm = try! Realm()
        try! realm.safeWrite{
            recipe.name = newData.name
            recipe.desc = newData.desc
            recipe.ingredients = newData.ingredients
            recipe.steps = newData.steps
            recipe.imageURL = newData.imageURL
            recipe.cateforyID = newData.cateforyID
        }
    }
    
    static func getRecipeOfCategoryID(_ id:String) -> [Recipe] {
        let realm = try! Realm()
        let result = realm.objects(Recipe.self).filter("cateforyID = %@", id).toArray(type: Recipe.self)
        
        return result
    }
}
