//
//  Recipe.swift
//  RecipeApp
//
//  Created by Tu (Tony) A. TRAN on 1/8/20.
//  Copyright © 2020 Tu (Tony) A. TRAN. All rights reserved.
//

import UIKit
import RealmSwift
import Foundation
import Network

class RecipeCategory: Object, Decodable {
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try (container.decodeIfPresent(String.self, forKey: .id) ?? "")
        self.name = try (container.decodeIfPresent(String.self, forKey: .name) ?? "")
    }
}

class Recipe: Object, Decodable {
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var imageURL = ""
    @objc dynamic var desc = ""
    @objc dynamic var cateforyID = ""
    
    var ingredients = List<Ingredient>()
    var steps = List<String>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    private enum CodingKeys: String, CodingKey {
        case name
        case imageURL
        case desc
        case ingredients
        case steps
        case cateforyID
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try (container.decodeIfPresent(String.self, forKey: .name) ?? "")
        self.imageURL = try (container.decodeIfPresent(String.self, forKey: .imageURL) ?? "")
        self.desc = try (container.decodeIfPresent(String.self, forKey: .desc) ?? "")
        self.cateforyID = try (container.decodeIfPresent(String.self, forKey: .cateforyID) ?? "")
        
        let ingredientsList = try container.decodeIfPresent([Ingredient].self, forKey: .ingredients) ?? []
        self.ingredients.append(objectsIn: ingredientsList)
        
        let stepsList = try container.decodeIfPresent([String].self, forKey: .steps) ?? []
        self.steps.append(objectsIn: stepsList)
    }
    
    func create() {
        RealmServices.createRecipe(self)
    }
    
    func save(_ copy:Recipe) {
        RealmServices.saveRecipe(self, newData: copy)
    }
}

class Ingredient: Object, Decodable {
    @objc dynamic var name = ""
    @objc dynamic var quantity = ""
    @objc dynamic var type = ""
    
    private enum CodingKeys: String, CodingKey {
        case name
        case quantity
        case type
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try (container.decodeIfPresent(String.self, forKey: .name) ?? "")
        self.quantity = try (container.decodeIfPresent(String.self, forKey: .quantity) ?? "")
        self.type = try (container.decodeIfPresent(String.self, forKey: .type) ?? "")
    }
}
